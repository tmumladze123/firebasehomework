package com.example.davalebacxra.Extensions

fun String.isEmailValid() : Boolean
{
    return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}
