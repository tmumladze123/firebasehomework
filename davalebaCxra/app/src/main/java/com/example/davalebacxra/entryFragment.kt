package com.example.davalebacxra

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.davalebacxra.databinding.FragmentEntryBinding
import com.example.davalebacxra.databinding.FragmentLogInBinding


class entryFragment : Fragment() {
    private lateinit var binding: FragmentEntryBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentEntryBinding.inflate(inflater,container,false)
        listen()
        return binding.root
    }

    fun listen()
    {
        binding.btnLogin.setOnClickListener{
            findNavController().navigate(R.id.action_entryFragment_to_logInFragment)
        }
        binding.btnRegister.setOnClickListener {
            findNavController().navigate(R.id.action_entryFragment_to_registerFragment)
        }
    }
}