package com.example.davalebacxra

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.davalebacxra.databinding.FragmentYourPageBinding
import com.google.firebase.auth.FirebaseAuth


class YourPageFragment : Fragment() {
    private lateinit var binding: FragmentYourPageBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentYourPageBinding.inflate(inflater,container,false)
        listen()
        return(binding.root)
    }
    fun listen()
    {
        binding.btnSignOut.setOnClickListener()
        {
            findNavController().navigate(R.id.action_global_entryFragment)
        }
    }
}