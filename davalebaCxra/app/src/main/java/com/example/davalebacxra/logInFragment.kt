package com.example.davalebacxra

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.davalebacxra.databinding.FragmentEntryBinding
import com.example.davalebacxra.databinding.FragmentLogInBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class logInFragment : Fragment() {
    lateinit var auth: FirebaseAuth
    private lateinit var binding: FragmentLogInBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentLogInBinding.inflate(inflater,container, false)
        auth = Firebase.auth
        listener()
        return binding.root
    }
    fun listener()
    {

        binding.btnBack.setOnClickListener{
            findNavController().navigate(R.id.action_logInFragment_to_entryFragment)
        }
        binding.btnNext.setOnClickListener {
            auth.signInWithEmailAndPassword(binding.etUser.text.toString(),binding.etPassword.text.toString())
                .addOnCompleteListener { result ->
                    if(result.isSuccessful)
                        findNavController().navigate(R.id.action_logInFragment_to_yourPageFragment)
                    else
                        Toast.makeText(context, "incorrect username or password", Toast.LENGTH_SHORT).show()
                }


        }
    }
}