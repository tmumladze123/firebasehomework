package com.example.davalebacxra

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.davalebacxra.databinding.FragmentRegisterNameBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class registerNameFragment() : Fragment() {
    lateinit var auth: FirebaseAuth
    private lateinit var binding: FragmentRegisterNameBinding
    lateinit  var email:String
    lateinit var password : String
    lateinit var name:String
    private lateinit var authentication:MainActivity
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentRegisterNameBinding.inflate(inflater,container,false)
        email= arguments?.getString("email").toString()
        password=arguments?.getString("password").toString()
        auth = Firebase.auth
        listener()
        return binding.root

    }
    fun listener()
    {
        binding.btnBack.setOnClickListener{
            findNavController().navigate(R.id.action_registerNameFragment_to_registerFragment)
        }
        binding.btnNext.setOnClickListener {
            if(binding.etUser.text.toString().isNotEmpty())
            {
                name=binding.etUser.text.toString()
                println(email + " emaili")
                println(password + " password")
                auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener {
                    result -> if(result.isSuccessful)
                    Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
                    else
                    Toast.makeText(context, "fail", Toast.LENGTH_SHORT).show()
                }
                findNavController().navigate(R.id.action_registerNameFragment_to_yourPageFragment)
            }
        }
    }

    }
