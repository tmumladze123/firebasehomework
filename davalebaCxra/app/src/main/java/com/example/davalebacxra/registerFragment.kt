package com.example.davalebacxra

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.davalebacxra.Extensions.isEmailValid
import com.example.davalebacxra.databinding.FragmentRegisterBinding
import com.google.android.material.snackbar.Snackbar
class registerFragment : Fragment() {
    private lateinit var binding: FragmentRegisterBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentRegisterBinding.inflate(inflater,container,false)
        listener()
        return binding.root
    }
    fun listener()
    {
        binding.btnBack.setOnClickListener{
        findNavController().navigate(R.id.action_registerFragment_to_entryFragment)
        }
        binding.btnNext.setOnClickListener{
            if(!binding.etPassword.text.toString().isEmpty() &&
               ! binding.etUser.text.toString().isEmpty()
                && binding.etUser.text.toString().isEmailValid()
                && binding.etPassword.text.toString().length>6)
            {
               // registerNameFragmentDirections.apply { user }
                val bundle = bundleOf("email" to binding.etUser.text.toString(),
                    "password" to binding.etPassword.text.toString(),
                    )
                findNavController().navigate(R.id.action_registerFragment_to_registerNameFragment,bundle)

            }
            else
             Snackbar.make(it,"Please enter correct INFO",Snackbar.LENGTH_LONG).show()
        }

    }

}